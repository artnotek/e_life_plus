import expressJwt from 'express-jwt';
import { getUserById } from '../src/helper/userHelpers';

function jwt() {
    const secret = 'mysecret94652';
    return expressJwt({ secret, isRevoked }).unless({
        path : [
            '/user/login',
            '/role',
            '/user/logout',
            '/user/create',
            '/partner/create',
        ],
    });
}

const isRevoked = async (req, payload, done) => {
    const user = await getUserById(payload.sub);
    if (user) {
        return done(null, true);
    }
    done();
};

export default jwt;