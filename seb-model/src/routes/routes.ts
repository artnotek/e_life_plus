import { Router } from 'express';
import AddressController from '../controller/AddressController';
import IngredientController from '../controller/IngredientController';
import IngredientMoldController from '../controller/IngredientMoldController';
import MoldController from '../controller/MoldController';
import PartnerRecoveryController from '../controller/PartnerRecoveryController';
import PartnershipRequestController from '../controller/PartnershipRequestController';
import RecoveryController from '../controller/RecoveryController';
import RewardController from '../controller/RewardController';
import RoleController from '../controller/RoleController';
import UserController from '../controller/UserController';
import MailController from '../controller/MailController';
import Auth from '../../config/auth';

export const router = Router();

router.get('/user', Auth.auth(['ROLE_USER', 'ROLE_PARTNER', 'ROLE_ADMIN']), UserController.getAllUsers);
router.get('/user/:id', Auth.auth(['ROLE_USER', 'ROLE_PARTNER', 'ROLE_ADMIN']), UserController.getUserById);
router.post('/user/create', UserController.insertCustomerAndAddress);
router.post('/user/login', UserController.login);
router.get('/user/logout', UserController.logout);
router.post('/user/passwordReset', UserController.resetPassword);
router.put('/user/:id', Auth.auth(['ROLE_USER', 'ROLE_PARTNER', 'ROLE_ADMIN']), UserController.updateUser);
router.delete('/user/:id', Auth.auth(['ROLE_ADMIN']), UserController.deleteUserWithAdress);

router.get('/address', Auth.auth(['ROLE_USER', 'ROLE_PARTNER', 'ROLE_ADMIN']), AddressController.getAllAddress);
router.get('/address/:id', Auth.auth(['ROLE_USER', 'ROLE_PARTNER', 'ROLE_ADMIN']), AddressController.getAddressByUsers);
router.put('/address/:id', Auth.auth(['ROLE_USER', 'ROLE_PARTNER', 'ROLE_ADMIN']), AddressController.updateAddressByIdUser);

router.get('/role', RoleController.getAllRoles);

router.get('/partner', Auth.auth(['ROLE_PARTNER', 'ROLE_ADMIN']), PartnershipRequestController.getAllPartnershipRequests);
router.get('/partner/:id', Auth.auth(['ROLE_PARTNER', 'ROLE_ADMIN']), PartnershipRequestController.getPartnershipRequestById);
router.post('/partner/create', PartnershipRequestController.insertPartnershipRequest);
router.put('/partner/:id/denied', Auth.auth(['ROLE_ADMIN']), PartnershipRequestController.partnerRequestDenied);
router.put('/partner/:id/granted', Auth.auth(['ROLE_ADMIN']), PartnershipRequestController.partnerRequestGranted);

router.get('/partnerRecovery', Auth.auth(['ROLE_PARTNER', 'ROLE_ADMIN']), PartnerRecoveryController.getAll);

router.get('/mold', Auth.auth(['ROLE_PARTNER', 'ROLE_ADMIN', 'ROLE_USER']), MoldController.getAll);
router.post('/mold', Auth.auth(['ROLE_PARTNER', 'ROLE_ADMIN', 'ROLE_USER']), MoldController.insert);
router.post('/mold/give', Auth.auth(['ROLE_PARTNER', 'ROLE_ADMIN', 'ROLE_USER']), MoldController.give);
router.post('/mold/take', Auth.auth(['ROLE_PARTNER', 'ROLE_ADMIN', 'ROLE_USER']), MoldController.take);
router.delete('/mold/:id', Auth.auth(['ROLE_PARTNER', 'ROLE_ADMIN', 'ROLE_USER']), MoldController.delete);

router.get('/recovery', Auth.auth(['ROLE_PARTNER', 'ROLE_ADMIN']), RecoveryController.getAll);
router.post('/recovery/pick', Auth.auth(['ROLE_PARTNER']), Auth.auth(['ROLE_PARTNER', 'ROLE_ADMIN']), RecoveryController.pick);
router.post('/recovery/validate', Auth.auth(['ROLE_PARTNER']), RecoveryController.validate);
router.post('/recovery/invalidate', Auth.auth(['ROLE_PARTNER']), RecoveryController.invalidate);
router.post('/recovery/leave', Auth.auth(['ROLE_PARTNER']), RecoveryController.leave);

router.get('/partnerRecovery', Auth.auth(['ROLE_PARTNER', 'ROLE_ADMIN']), PartnerRecoveryController.getAll);

router.get('/ingredient', Auth.auth(['ROLE_PARTNER', 'ROLE_ADMIN', 'ROLE_USER']), IngredientController.getAll);

router.get('/ingredientMold', Auth.auth(['ROLE_PARTNER', 'ROLE_ADMIN', 'ROLE_USER']), IngredientMoldController.getAll);

router.get('/reward', Auth.auth(['ROLE_PARTNER', 'ROLE_ADMIN', 'ROLE_USER']), RewardController.getAllRewards);
router.post('/reward', Auth.auth(['ROLE_ADMIN']), RewardController.insertReward);
router.delete('/reward/:id', Auth.auth(['ROLE_PARTNER', 'ROLE_ADMIN', 'ROLE_USER']), RewardController.deleteReward);
router.put('/reward/:id', Auth.auth(['ROLE_PARTNER', 'ROLE_ADMIN', 'ROLE_USER']), RewardController.updateReward);

router.post('/sendMail', Auth.auth(['ROLE_PARTNER', 'ROLE_ADMIN', 'ROLE_USER']), MailController.sendMail);