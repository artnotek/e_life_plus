import { INormalizedData } from '../../../seb-model/src/interface/INormalizedData';
import { IPartnerRecovery } from '../../../seb-model/src/interface/IPartnerRecovery';
import { IRecovery } from '../../../seb-model/src/interface/IRecovery';
import { IUser } from '../../../seb-model/src/interface/IUser';
import { Identifier } from '../../../seb-model/src/type/Identifier';
import { RecoveryState } from '../../../seb-model/src/type/RecoveryState';
import { filter } from '../utils';

interface IStoreState {
    users : INormalizedData<IUser>;
    recoveries : INormalizedData<IRecovery>;
    partnerRecoveries : INormalizedData<IPartnerRecovery>;
}

export const getUserTotalMoldPicked = (state : IStoreState) => (idUser : Identifier) : number => {
    const partnerRecoveries = filter(state.partnerRecoveries)(partnerRecovery => partnerRecovery.idUser === idUser);
    const partnerRecoveryIds = partnerRecoveries.map(partnerRecovery => partnerRecovery.idRecovery);
    return filter(state.recoveries)(recovery => partnerRecoveryIds.includes(recovery.id) && recovery.recoveryState === RecoveryState.Delivered).length;
};