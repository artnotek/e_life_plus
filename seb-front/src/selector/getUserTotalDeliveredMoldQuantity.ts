import { IMold } from '../../../seb-model/src/interface/IMold';
import { INormalizedData } from '../../../seb-model/src/interface/INormalizedData';
import { IRecovery } from '../../../seb-model/src/interface/IRecovery';
import { IUser } from '../../../seb-model/src/interface/IUser';
import { Identifier } from '../../../seb-model/src/type/Identifier';
import { listUserDeliveredMolds } from './listUserDeliveredMolds';

interface IStoreState {
    users : INormalizedData<IUser>;
    molds : INormalizedData<IMold>;
    recoveries : INormalizedData<IRecovery>;
}

export const getUserTotalDeliveredMoldQuantity = (state : IStoreState) => (idUser : Identifier) : number => {
    const molds = listUserDeliveredMolds(state)(idUser);
    return molds.reduce((nb, mold) => {
        return nb + mold.quantity;
    }, 0);
};