import { Dispatch } from 'redux';
import { IAddress } from '../../../seb-model/src/interface/IAddress';
import { IIngredient } from '../../../seb-model/src/interface/IIngredient';
import { IIngredientMold } from '../../../seb-model/src/interface/IIngredientMold';
import { IMold } from '../../../seb-model/src/interface/IMold';
import { INormalizedData } from '../../../seb-model/src/interface/INormalizedData';
import { IPartnerRecovery } from '../../../seb-model/src/interface/IPartnerRecovery';
import { IPartnershipRequest } from '../../../seb-model/src/interface/IPartnershipRequest';
import { IRecovery } from '../../../seb-model/src/interface/IRecovery';
import { IReward } from '../../../seb-model/src/interface/IReward';
import { IRole } from '../../../seb-model/src/interface/IRole';
import { IUser } from '../../../seb-model/src/interface/IUser';
import { setAddresses } from '../action-creator/setAddresses';
import { setIngredientMolds } from '../action-creator/setIngredientMolds';
import { setIngredients } from '../action-creator/setIngredients';
import { setMolds } from '../action-creator/setMolds';
import { setPartnerRecoveries } from '../action-creator/setParnerRecoveries';
import { setPartnershipRequests } from '../action-creator/setPartnershipRequests';
import { setRecoveries } from '../action-creator/setRecoveries';
import { setRewards } from '../action-creator/setRewards';
import { setRoles } from '../action-creator/setRoles';
import { setUsers } from '../action-creator/user/setUsers';

export const loadAllData = (dispatch : Dispatch) => ({
    setUsers : (users : INormalizedData<IUser>) => dispatch(setUsers(users)),
    setPartnershipRequests : (partnershipRequests : INormalizedData<IPartnershipRequest>) => dispatch(setPartnershipRequests(partnershipRequests)),
    setAddresses : (addresses : INormalizedData<IAddress>) => dispatch(setAddresses(addresses)),
    setRoles : (roles : INormalizedData<IRole>) => dispatch(setRoles(roles)),
    setMolds : (molds : INormalizedData<IMold>) => dispatch(setMolds(molds)),
    setRecoveries : (recoveries : INormalizedData<IRecovery>) => dispatch(setRecoveries(recoveries)),
    setPartnerRecoveries : (partnerRecoveries : INormalizedData<IPartnerRecovery>) => dispatch(setPartnerRecoveries(partnerRecoveries)),
    setRewards : (rewards : INormalizedData<IReward>) => dispatch(setRewards(rewards)),
    setIngredients : (ingredients : INormalizedData<IIngredient>) => dispatch(setIngredients(ingredients)),
    setIngredientMolds : (ingredientMolds : INormalizedData<IIngredientMold>) => dispatch(setIngredientMolds(ingredientMolds)),
});