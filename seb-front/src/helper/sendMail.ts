import { IMail } from '../../../seb-model/src/interface/IMail';

export const contactUser = async (body : IMail) => {
    const init = {
        method : 'post',
        headers : {
            'Content-Type' : 'application/json',
            'Authorization' : `Bearer ${ localStorage.getItem('token') }`
        },
        body : JSON.stringify(body)
    };
    return await fetch(`http://localhost:3000/sendMail`, init);
};