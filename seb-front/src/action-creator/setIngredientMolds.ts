import { IIngredientMold } from '../../../seb-model/src/interface/IIngredientMold';
import { INormalizedData } from '../../../seb-model/src/interface/INormalizedData';

export const SetIngredientMolds = 'ingredientMolds/set-ingredientMolds';

export const setIngredientMolds = (ingredientMolds : INormalizedData<IIngredientMold>) => {
    return {
        type : SetIngredientMolds,
        payload : { ingredientMolds }
    };
};