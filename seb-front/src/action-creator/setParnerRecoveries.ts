import { INormalizedData } from '../../../seb-model/src/interface/INormalizedData';
import { IPartnerRecovery } from '../../../seb-model/src/interface/IPartnerRecovery';

export const SetPartnerRecoveries = 'partner-recoveries/set-partner-recoveries';

export const setPartnerRecoveries = (partnerRecoveries : INormalizedData<IPartnerRecovery>) => {
    return {
        type : SetPartnerRecoveries,
        payload : { partnerRecoveries}
    };
};