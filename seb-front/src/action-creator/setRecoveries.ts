import { INormalizedData } from '../../../seb-model/src/interface/INormalizedData';
import { IRecovery } from '../../../seb-model/src/interface/IRecovery';

export const SetRecoveries = 'recoveries/set-recoveries';

export const setRecoveries = (recoveries : INormalizedData<IRecovery>) => {
    return {
        type : SetRecoveries,
        payload : { recoveries }
    };
};