import { INormalizedData } from '../../../../seb-model/src/interface/INormalizedData';
import { IUser } from '../../../../seb-model/src/interface/IUser';

export const SetUsers = 'users/set-users';

export const setUsers = (users : INormalizedData<IUser>) => {
    return {
        type : SetUsers,
        payload : { users }
    };
};