import Service from './Service';

const baseUrl = 'http://localhost:3000/partnerRecovery';

const PartnerRecoveryService = new Service(baseUrl);

export default PartnerRecoveryService;