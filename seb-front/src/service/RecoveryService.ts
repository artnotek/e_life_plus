import { postData } from '../helper/postData';
import Service from './Service';

const baseUrl = 'http://localhost:3000/recovery';

class RecoveryService extends Service {
    constructor(baseUrl) {
        super(baseUrl);
    }

    async pick(body) {
        return await fetch(`${ baseUrl }/pick`, postData(body));
    }

    async leave(body) {
        return await fetch(`${ baseUrl }/leave`, postData(body));
    }

    async validate(body) {
        return await fetch(`${ baseUrl }/validate`, postData(body));
    }

    async invalidate(body) {
        return await fetch(`${ baseUrl }/invalidate`, postData(body));
    }

}

const RecoveryServiceInstance = new RecoveryService(baseUrl);

export default RecoveryServiceInstance;