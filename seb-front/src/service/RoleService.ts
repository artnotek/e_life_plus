import Service from './Service';

const baseUrl = 'http://localhost:3000/role';

const RoleService = new Service(baseUrl);

export default RoleService;