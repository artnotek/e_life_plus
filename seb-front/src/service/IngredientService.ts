import Service from './Service';

const baseUrl = 'http://localhost:3000/ingredient';

const IngredientService = new Service(baseUrl);

export default IngredientService;