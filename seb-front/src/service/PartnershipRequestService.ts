import { RequestState } from '../../../seb-model/src/type/RequestState';
import { postData } from '../helper/postData';
import Service from './Service';

const baseUrl = 'http://localhost:3000/partner';

class PartnershipRequestService extends Service {
    constructor(baseUrl) {
        super(baseUrl);
    }

    public async create(body) {
        return await fetch(`${ this.baseUrl }/create`, postData(body));
    }

    public async denyOrGrant(id, option : RequestState) {
        const init = {
            method : 'PUT',
            headers : {
                'Content-Type' : 'application/json',
                'Authorization' : `Bearer ${ localStorage.getItem('token') }`
            }
        };
        const route = option === RequestState.Granted ? 'granted' : 'denied';
        return await fetch(`${ this.baseUrl }/${ id }/${ route }`, init);
    }

}

const PartnershipRequestServiceInstance = new PartnershipRequestService(baseUrl);

export default PartnershipRequestServiceInstance;