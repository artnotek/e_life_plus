import Service from './Service';

const baseUrl = 'http://localhost:3000/mold';

const MoldService = new Service(baseUrl);

export default MoldService;