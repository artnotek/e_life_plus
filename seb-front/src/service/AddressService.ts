import Service from './Service';

const baseUrl = 'http://localhost:3000/address';

const AddressService = new Service(baseUrl);

export default AddressService;